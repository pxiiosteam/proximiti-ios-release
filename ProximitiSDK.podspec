Pod::Spec.new do |spec|
spec.name         = "ProximitiSDK"
spec.version      = "3.1"
spec.summary      = "Proximiti SDK enables any app to become location aware and deliver in-moment experiences"
spec.homepage     = "https://www.proximiti.asia"
spec.author       = { "Proximiti" => "ios@proximiti.com.au" }
spec.platform     = :ios, '8.0'
spec.source       = { :git => "https://bitbucket.org/pxiiosteam/proximiti-ios-release.git", :tag => "3.1" }
spec.vendored_frameworks = 'Framework/ProximitiSDK.framework'
end
