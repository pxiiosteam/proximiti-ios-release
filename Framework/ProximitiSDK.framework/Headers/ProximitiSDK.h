/*
   Proximiti SDK header file 3.1
   Copyright (c) 2017 Proximiti. All rights reserved.
 */
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@import CoreLocation;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 100000
@import UserNotifications;
#endif
@protocol ProximitiSDKDelegate;

@interface ProximitiSDK : NSObject
+ (instancetype)sharedClient;
@property (nonatomic, weak) id<ProximitiSDKDelegate> delegate;

//dictionary keys can be passed are key,host and endpoint-url is required
-(void)setConfig:(NSDictionary*)pxiDict;
-(void)startMonitoringStartSdkForeground:(BOOL)start StartInBackground:(BOOL)bgStart; //Main Entry point: start the SDK monitoring fns pass yes or no to configure the running mode of SDK
-(void)terminateSdk; //terminate SDK
-(void)loadViewOnNotificationClick:(UILocalNotification *)notification startSdkForeground:(BOOL) start StartSdkBackground:(BOOL)bgStart; //loads webview notification on local notification click and pass yes or NO to start SDK
-(void)appLaunchedByClickOnNotification:(UILocalNotification *)notification startSdKForeground:(BOOL) start StartSdkBackground:(BOOL)bgStart;//application launched by the click on the local notification when notInMemory and pass YES or NO to start SDK
-(void)setForegroundMode:(BOOL)active backgroundMode:(BOOL)backgroundActive; //set or reset location manager ranging properties in active or background modes
-(void)clearProperty:(NSString*)propToClear; //clear the SDK properties pass the notificatioUrl or notificationDict
-(void)setSegments:(NSDictionary*)segementData; //use to set segments
-(void)clearSegments; //use to remove the stored segments
-(void)passNotificationToDelegate:(NSMutableDictionary*)notification;
-(NSDictionary*)getDFPTags; //used for custom targeting the DFP ads
-(void)getPreciseLocation; //get Precise location
-(NSString*)getPxiUserLocation;
-(NSString*)getSDKVersion; //get SDK version
-(void)setSessionId:(NSString*)identifierToUse;//set new session id
-(void)updateSessionId:(NSString*)identifierToUse;//update the session id and carry forward the campaign history
@property (nonatomic, copy) NSString *notificationUrl; //notificationUrl
@property (nonatomic, copy) NSMutableDictionary *notificationDict; //notificationUrlDictionary have properties when user click on redeem button on the notification web view
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 100000
-(void)loadViewOnUNNotificationClick:(UNNotification *)response startSdkForeground:(BOOL) start StartSdkBackground:(BOOL)bgStart; //UNUserNotification implementation for ios 10 or above
#endif

@end
// In this header, you should import all the public headers of your framework using statements like #import <ProximitiSDK/PublicHeader.h>

@protocol ProximitiSDKDelegate <NSObject>
//delegate method provides a callback on receiving the deep link notification
@optional
- (void)didReceiveDeepLinkNotification:(NSString *)notification;

@end // end of delegate protocol
